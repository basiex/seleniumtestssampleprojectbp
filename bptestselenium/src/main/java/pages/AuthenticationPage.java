package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utils;

public class AuthenticationPage {

    private WebDriver driver;
    private Utils utils;

    public AuthenticationPage(WebDriver driver)
    {
        this.driver = driver;
        this.utils = new Utils(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[@class='navigation_page'and contains(text(), 'Authentication')]")
    private WebElement myAccountNavBar;

    @FindBy(xpath = "//input[@type='text'][@name='email_create']")
    private WebElement createAccountEmailField;

    @FindBy(id = "SubmitCreate")
    private WebElement createAccountButton;

    @FindBy(id = "create_account_error")
    private WebElement createAccountError;

    @FindBy(id = "email")
    private WebElement loginEmailField;

    @FindBy(id = "passwd")
    private WebElement loginPasswordField;

    @FindBy(id = "SubmitLogin")
    private WebElement loginButton;

    public WebElement getMyAccountNavBar() {
        return myAccountNavBar;
    }

    public WebElement getCreateAccountEmailField() {
        return createAccountEmailField;
    }

    public void createAccountEmailFieldClick() {
        createAccountEmailField.click();
    }

    public void createAccountButtonClick() {
        createAccountButton.click();
    }

    public void enterTextToCreateAccountEmailField(String text) {
        utils.enterText(createAccountEmailField, text);
    }

    public void enterTextToLoginEmailField(String text) {
        utils.enterText(loginEmailField, text);
    }

    public void enterTextToLoginPasswordField(String text) {
        utils.enterText(loginPasswordField, text);
    }

    public void loginButtonClick() {
        loginButton.click();
    }

}
