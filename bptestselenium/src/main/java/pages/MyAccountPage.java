package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage {

    private WebDriver driver;

    public MyAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='header_user_info']/a[contains(@title, 'View my customer account')]")
    private WebElement loggedUserMenuItem;

    @FindBy(xpath = "//span[@class='navigation_page'][text()='My account']")
    private WebElement myAccountNavBar;

    @FindBy(xpath = "//span[text()='Order history and details']")
    private WebElement orderHistoryDetailsButton;

    @FindBy(xpath = "//span[text()='My credit slips']")
    private WebElement creditSlipsButton;

    @FindBy(xpath = "//span[text()='My addresses']")
    private WebElement addressesButton;

    @FindBy(xpath = "//span[text()='//span[text()='My personal information']")
    private WebElement personalInfoButton;

    @FindBy(xpath = "//span[text()='My wishlists']")
    private WebElement wishlistButton;

    public void myAddressesMenuItemClick() {
        addressesButton.click();
    }

}
