package pages;

import blocks.CustomerAddressBlock;
import blocks.CustomerPersonalInfoBlock;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage {

    private WebDriver driver;
    private CustomerPersonalInfoBlock customerPersonalInfoBlock;
    private CustomerAddressBlock customerAddressBlock;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        customerPersonalInfoBlock = new CustomerPersonalInfoBlock(driver);
        customerAddressBlock = new CustomerAddressBlock(driver);
    }

    public CustomerPersonalInfoBlock getCustomerPersonalInfoBlock() {
        return customerPersonalInfoBlock;
    }

    public CustomerAddressBlock getCustomerAddressBlock() {
        return customerAddressBlock;
    }
}
