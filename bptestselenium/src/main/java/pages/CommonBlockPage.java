package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonBlockPage {
    public WebDriver driver;

    public CommonBlockPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='banner']//div//div/a/img[@class='img-responsive']")
    private WebElement bannerImage;

    @FindBy(id = "contact-link")
    private WebElement contactUsMenuItem;

    @FindBy(xpath = "//div[@class='nav']//div//div/nav//a[@class='login']")
    private WebElement signInMenuItem;

    @FindBy(xpath = "//div[@class='nav']//div//div/nav//a[@class='logout']")
    private WebElement signOutMenuItem;

    @FindBy(id = "search_query_top")
    private WebElement searchTextField;

    @FindBy(xpath = "//div[@class='shopping_cart']/a[contains(@title, 'View my shopping cart')]")
    private WebElement viewShoppingCardBanner;

    public WebElement getSignInMenuItem() {
        return signInMenuItem;
    }

    public void signInMenuItemClick() {
        signInMenuItem.click();
    }

    public void signOutMenuItemClick() {
        signOutMenuItem.click();
    }

}
