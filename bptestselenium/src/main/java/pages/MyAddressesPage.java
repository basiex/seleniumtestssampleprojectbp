package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utils;

public class MyAddressesPage {

    private WebDriver driver;
    private Utils utils;

    public MyAddressesPage(WebDriver driver) {
        this.driver = driver;
        this.utils = new Utils(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[contains(@title, 'Add an address')]")
    private WebElement addNewAddressButton;

    @FindBy(xpath = "//ul[@class='last_item alternate_item box']/li/h3")
    private WebElement secondAddressName;

    public void addNewAddressButtonClick() {
        addNewAddressButton.click();
    }

    public WebElement getSecondAddressName() {
        return secondAddressName;
    }

    public void shouldSeeSecondAddressName(String text) {
        utils.shouldSeeText(secondAddressName, text);
    }
}
