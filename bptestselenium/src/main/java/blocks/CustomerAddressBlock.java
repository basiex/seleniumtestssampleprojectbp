package blocks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import utils.Utils;

public class CustomerAddressBlock {

    private WebDriver driver;
    private Utils utils;

    public CustomerAddressBlock(WebDriver driver) {
        this.driver = driver;
        this.utils = new Utils(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(id = "firstname")
    private WebElement customerAddressFirstNameTextField;

    @FindBy(id = "lastname")
    private WebElement customerAddressLastNameTextField;

    @FindBy(id = "company")
    private WebElement customerAddressCompanyTextField;

    @FindBy(id = "address1")
    private WebElement customerAddressLine1TextField;

    @FindBy(id = "address2")
    private WebElement customerAddressLine2TextField;

    @FindBy(id = "city")
    private WebElement customerAddressCityTextField;

    @FindBy(id = "id_state")
    private WebElement customerAddressStateDropdown;

    @FindBy(id = "postcode")
    private WebElement customerAddressPostcodeTextField;

    @FindBy(id = "id_country")
    private WebElement customerAddressCountryDropdown;

    @FindBy(id = "other")
    private WebElement customerAddressOtherTextField;

    @FindBy(id = "phone")
    private WebElement customerAddressHomePhoneTextField;

    @FindBy(id = "phone_mobile")
    private WebElement customerAddressMobilePhoneTextField;

    @FindBy(id = "alias")
    private WebElement customerAddressAliasTextField;

    @FindBy(id = "submitAccount")
    private WebElement registerButton;

    @FindBy(id = "submitAddress")
    private WebElement saveAddressButton;


    public void enterTextToAddressFirstNameTextField(String text) {
        utils.enterText(customerAddressFirstNameTextField, text);
    }

    public void enterTextToAddressLastNameTextField(String text) {
        utils.enterText(customerAddressLastNameTextField, text);
    }

    public void enterTextToAddressCompanyTextField(String text) {
        utils.enterText(customerAddressCompanyTextField, text);
    }

    public void enterTextToAddressAddressLine1TextField(String text) {
        utils.enterText(customerAddressLine1TextField, text);
    }

    public void enterTextToAddressAddressLine2TextField(String text) {
        utils.enterText(customerAddressLine2TextField, text);
    }

    public void enterTextToAddressAddressCityTextField(String text) {
        utils.enterText(customerAddressCityTextField, text);
    }

    public void enterTextToAddressPostcodeTextField(String text) {
        utils.enterText(customerAddressPostcodeTextField, text);
    }

    public void enterTextToAddressOtherTextField(String text) {
        utils.enterText(customerAddressOtherTextField, text);
    }

    public void enterTextToAddressHomePhoneTextField(String text) {
        utils.enterText(customerAddressHomePhoneTextField, text);
    }

    public void enterTextToAddressMobliePhoneTextField(String text) {
        utils.enterText(customerAddressMobilePhoneTextField, text);
    }

    public void enterTextToAddressAliasTextField(String text) {
        utils.enterText(customerAddressAliasTextField, text);
    }

    public void stateDropdownClickAndSelect(String text) {
        customerAddressStateDropdown.click();
        Select dropdown = new Select(customerAddressStateDropdown);
        dropdown.selectByVisibleText(text);
    }

    public void countryDropdownClickAndSelect(String text) {
        customerAddressCountryDropdown.click();
        Select dropdown = new Select(customerAddressCountryDropdown);
        dropdown.selectByVisibleText(text);
    }

    public void registerButtonClick() {
        utils.clickElement(registerButton);
    }

    public void saveButtonClick() {
        utils.clickElement(saveAddressButton);
    }

    public void fillInCustomerAddressData (String company, String addressLine1, String addressLine2, String city, String stateSelect, String postCode, String countryDropdown, String other, String homePhone, String mobilePhone, String addressAlias) {
        enterTextToAddressCompanyTextField(company);
        enterTextToAddressAddressLine1TextField(addressLine1);
        enterTextToAddressAddressLine2TextField(addressLine2);
        enterTextToAddressAddressCityTextField(city);
        stateDropdownClickAndSelect(stateSelect);
        enterTextToAddressPostcodeTextField(postCode);
        countryDropdownClickAndSelect(countryDropdown);
        enterTextToAddressOtherTextField(other);
        enterTextToAddressHomePhoneTextField(homePhone);
        enterTextToAddressMobliePhoneTextField(mobilePhone);
        enterTextToAddressAliasTextField(addressAlias);
    }

}
