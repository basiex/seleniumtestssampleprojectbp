package blocks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import utils.Utils;


public class CustomerPersonalInfoBlock {

    private WebDriver driver;
    private Utils utils;

    public CustomerPersonalInfoBlock(WebDriver driver) {
        this.driver = driver;
        this.utils = new Utils(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "id_gender1")
    private WebElement genderMrRadio;

    @FindBy(id = "id_gender2")
    private WebElement genderMrsRadio;

    @FindBy(id = "customer_firstname")
    private WebElement customerFirstNamePersonalTextField;

    @FindBy(id = "customer_lastname")
    private WebElement customerLastNamePersonalTextField;

    @FindBy(id = "email")
    private WebElement customerEmailTextField;

    @FindBy(id = "passwd")
    private WebElement customerPasswordTextField;

    @FindBy(id = "days")
    private WebElement customerBirthDaysDropdown;

    @FindBy(id = "months")
    private WebElement customerBirthMonthsDropdown;

    @FindBy(id = "years")
    private WebElement customerBirthYearsDropdown;

    @FindBy(id = "uniform-newsletter")
    private WebElement newsletterCheckbox;

    @FindBy(id = "uniform-optin")
    private WebElement specialOffersCheckbox;

    @FindBy(id = "submitAccount")
    private WebElement registerButton;

    public WebElement getGenderMrRadio() {
        return genderMrRadio;
    }

    public void genderMrRadioClick() {
        utils.clickElement(genderMrRadio);
    }

    public void newsletterCheckboxClick() {
        utils.clickElement(newsletterCheckbox);
    }

    public void specialOffersCheckboxClick() {
        utils.clickElement(specialOffersCheckbox);
    }

    public void customerBirthDaysDropdownClickAndSelect(String text) {
        customerBirthDaysDropdown.click();
        Select dropdown = new Select(customerBirthDaysDropdown);
        dropdown.selectByValue(text);
    }

    public void customerBirthMonthsDropdownClickAndSelect(String text) {
        customerBirthMonthsDropdown.click();
        Select dropdown = new Select(customerBirthMonthsDropdown);
        dropdown.selectByValue(text);
    }

    public void setCustomerBirthYearsDropdownClickAndSelect(String text) {
        customerBirthYearsDropdown.click();
        Select dropdown = new Select(customerBirthYearsDropdown);
        dropdown.selectByValue(text);
    }

    public void enterTextToFirstNamePersonalTextField(String text) {
        utils.enterText(customerFirstNamePersonalTextField, text);
    }

    public void enterTextToLastNamePersonalTextField(String text) {
        utils.enterText(customerLastNamePersonalTextField, text);
    }

    public void enterTextToPasswordPersonalTextField(String text) {
        utils.enterText(customerPasswordTextField, text);
    }

    public void fillInCustomerPersonalData(String firstName, String lastName, String password, String day, String monthNo, String year) {
        genderMrRadioClick();
        enterTextToFirstNamePersonalTextField(firstName);
        enterTextToLastNamePersonalTextField(lastName);
        enterTextToPasswordPersonalTextField(password);
        customerBirthDaysDropdownClickAndSelect(day);
        customerBirthMonthsDropdownClickAndSelect(monthNo);
        setCustomerBirthYearsDropdownClickAndSelect(year);
        newsletterCheckboxClick();
        specialOffersCheckboxClick();
    }

}
