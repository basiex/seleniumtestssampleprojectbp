package entities;

import utils.Utils;

public class User {
    private String email;
    private String password;


    public User() {
        this.email = Utils.generateRandomEmail();
        this.password = "test1234";
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
