package utils;

import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;


public class Utils {

    private WebDriver driver;
    private Waiters waiters;

    public Utils(WebDriver driver) {
        this.driver = driver;
        this.waiters = new Waiters(driver);
    }

    public void enterText(WebElement element, String text) {
        waiters.waitForElementPresence(element);
        element.clear();
        element.sendKeys(text);
    }

    public void clickElement(WebElement element) {
        waiters.waitForElementPresence(element);
        element.click();
    }

    public void shouldSeeText(WebElement element, String text) {
        waiters.waitForElementPresence(element);
        Assert.assertEquals("Text: " + text + "not found.", element.getText().toUpperCase(), text.toUpperCase());
    }

    public static String generateRandomEmail() {
        int randomNumber = new Random().nextInt(999);
        return "anikitama+" + randomNumber + "@gmail.com";
    }
}
