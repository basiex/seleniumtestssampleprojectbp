import entities.User;
import org.junit.Test;

public class UserAccountTests extends TestEnvironment {

    User user = new User();

    @Test
    public void SuccessfulSignUpWithFullUserDataTest() {
        commonBlockPage.signInMenuItemClick();
        authenticationPage.enterTextToCreateAccountEmailField(user.getEmail());
        authenticationPage.createAccountButtonClick();
        //Methods below are not elegant, but I wanted to show that code (where filling every field is done in separate line using getters)
        //is too long and needs refactor.
        createAccountPage.getCustomerPersonalInfoBlock().fillInCustomerPersonalData(
                "John",
                "Smith",
                user.getPassword(),
                "14",
                "3",
                "1999");
        createAccountPage.getCustomerAddressBlock().fillInCustomerAddressData(
                "A company",
                "Address Line 1",
                "Address Line 2",
                "New York",
                "Colorado",
                "12345",
                "United States",
                "Some additional info.",
                "123-123-123",
                "444555666",
                "My main address");
        createAccountPage.getCustomerAddressBlock().registerButtonClick();
    }

    @Test
    public void SuccessfulSignInTest() {
        commonBlockPage.signInMenuItemClick();
        authenticationPage.enterTextToCreateAccountEmailField(user.getEmail());
        authenticationPage.createAccountButtonClick();
        createAccountPage.getCustomerPersonalInfoBlock().fillInCustomerPersonalData(
                "Marcus",
                "Miller",
                user.getPassword(),
                "25",
                "3",
                "1985");
        createAccountPage.getCustomerAddressBlock().fillInCustomerAddressData(
                "A company",
                "Address Line 1",
                "Address Line 2",
                "New York",
                "Colorado",
                "12345",
                "United States",
                "",
                "555-777-888",
                "999000999",
                "My main address");
        createAccountPage.getCustomerAddressBlock().registerButtonClick();
        commonBlockPage.signOutMenuItemClick();
        commonBlockPage.signInMenuItemClick();
        authenticationPage.enterTextToLoginEmailField(user.getEmail());
        authenticationPage.enterTextToLoginPasswordField(user.getPassword());
        authenticationPage.loginButtonClick();
    }

    @Test
    public void AddAnotherAddressInCustomerAccount() {
        commonBlockPage.signInMenuItemClick();
        authenticationPage.enterTextToCreateAccountEmailField(user.getEmail());
        authenticationPage.createAccountButtonClick();
        createAccountPage.getCustomerPersonalInfoBlock().fillInCustomerPersonalData(
                "Marcus",
                "Miller",
                user.getPassword(),
                "25",
                "3",
                "1985");
        createAccountPage.getCustomerAddressBlock().fillInCustomerAddressData(
                "A company",
                "Address Line 1",
                "Address Line 2",
                "New York",
                "Colorado",
                "12345",
                "United States",
                "",
                "555-777-888",
                "999000999",
                "My main address");
        createAccountPage.getCustomerAddressBlock().registerButtonClick();
        myAccountPage.myAddressesMenuItemClick();
        myAddressesPage.addNewAddressButtonClick();
        createAccountPage.getCustomerAddressBlock().fillInCustomerAddressData(
                "A company111111",
                "Address Line 1",
                "Address Line 2",
                "New York",
                "Colorado",
                "12345",
                "United States",
                "",
                "555-777-888",
                "999000999",
                "My second address");
        createAccountPage.getCustomerAddressBlock().saveButtonClick();
        myAddressesPage.shouldSeeSecondAddressName("My second address");
    }
}
