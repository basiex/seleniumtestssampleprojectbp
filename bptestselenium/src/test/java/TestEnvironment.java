import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;
import utils.Utils;
import utils.Waiters;

public class TestEnvironment {

    private WebDriver driver = new ChromeDriver();
    protected Utils utils = new Utils(driver);
    Waiters waiters = new Waiters(driver);
    CommonBlockPage commonBlockPage = new CommonBlockPage(driver);
    AuthenticationPage authenticationPage = new AuthenticationPage(driver);
    CreateAccountPage createAccountPage = new CreateAccountPage(driver);
    MyAccountPage myAccountPage = new MyAccountPage(driver);
    MyAddressesPage myAddressesPage = new MyAddressesPage(driver);

    @Before
    public void enterPage() {
        driver.get("http://automationpractice.com/index.php");
        waiters.waitForElementPresence(commonBlockPage.getSignInMenuItem());
    }

    @After
    public void quitBrowser() {
        driver.quit();
    }

}
